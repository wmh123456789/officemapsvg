struct Node{
    int id;
    int x;
    int y;
};

struct Edge{
    int startid;
    int endid;
    float distance;
};

struct Graph{
    Edge* edges;
    Node* nodes;
};

// 输出邻接表示例
// a: a 0.0 b 3.0 c 1.0
// b: a 3.0 b 0.0
// c: a 1.0 c 0.0