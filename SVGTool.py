import json
import xml.etree.ElementTree as ET

from svg.path import parse_path

from CheckUnion import *
from Dijkstra import dijkstra
from csvTool import *

# Parameters
NS0 = "{http://www.w3.org/2000/svg}"
NS_ink = "{http://www.inkscape.org/namespaces/inkscape}"

# EPS = 140.0  # for HDBC3
EPS = 4.0  # for Testline
# For HDBC3: kx=29600, ky=21400, dx=0, dy=0, theta=0.2, r=60
MTKX = 29600
MTKY = 21400
MTDX = 0
MTDY = 0
MTTHETA = 0.2
MAX = float('inf')
EPSDict = {
    'TestLine.svg': 4.0,
    'HDBC3.svg': 100.0,
    'HDBC3a.svg': 100.0,
    'WJE2.svg': 150.0
}


def checkGraphLinked(graph, mode='poi'):
    data = graph.dumpGraph()
    if mode == 'poi':
        nodes = graph.getPOIs()
    else:
        nodes = graph.getNodes()
    loopCnt = 0

    for i, n in enumerate(nodes):
        print('Checking node: {}, {}/{}'.format(n.id, i, len(nodes)))
        dist, path, t = dijkstra(data, n.id)
        loopCnt += t
        nonlink_nodes = [p for p in dist if dist[p] == float('inf')]
        if nonlink_nodes:
            print('{} nodes in graph, {} loops for calc.'.format(
                len(nodes), loopCnt
            ))
            print('Find nonlink node:{}'.format(nonlink_nodes))
            return False

    print('{} nodes in graph, {} loops for calc.'.format(
        len(nodes), loopCnt
    ))
    return True


def LoadSVG(filename):
    return ET.parse(filename)


def LoadElementInLayer(lyrName, root):
    elemList = []
    for lyr in root.findall(".//*[@" + NS_ink + "label='" + lyrName + "']"):
        elemList += [e for e in lyr]
    return elemList


def Ellipse2circle(root, r=2):
    for c in root.iter(NS0 + 'ellipse'):
        c.set('r', str(r))
        c.tag = NS0 + 'circle'
        c.attrib.pop('rx')
        c.attrib.pop('ry')
        # print(c.tag,c.attrib.keys())
    pass


def UnifyCircle(root, r=2):
    for c in root.iter(NS0 + 'circle'):
        c.set('r', str(r))
        # print(c.attributes)
    pass


def M2PointList(d):
    pass


def Path2Polyline(pathelem):
    d = pathelem.attrib['d']
    points_txt = ""

    path = parse_path(d)
    for node in path:
        if 'Move' in str(node) or 'Line' in str(node):
            points_txt += str(node.end.real) + ',' + str(node.end.imag) + ' '
        else:
            print('ERROR! cannot support: ' + d)
            return
    pathelem.tag = NS0 + 'polyline'
    pathelem.attrib.pop('d')
    pathelem.set('points', points_txt)
    return points_txt


def drawPolyline(polylineElem, pnlist, style=''):
    if not style:
        style = "fill:none;" \
                "stroke:#ff00ff;" \
                "stroke-width:105.0px;" \
                "stroke-linecap:butt;" \
                "stroke-linejoin:miter;" \
                "stroke-opacity:1"
    points_txt = ''
    for pn in pnlist:
        points_txt += '{:.3f},{:.3f} '.format(pn.x, pn.y)
    polylineElem.set('points', points_txt)
    polylineElem.set('style', style)
    return points_txt


def drawPOI(circleElem, poi, r=60.0, style=''):
    if not style:
        style = "clip-rule:evenodd;" \
                "display:inline;" \
                "opacity:0.93900003;" \
                "fill:#ff2200;" \
                "fill-opacity:1;" \
                "fill-rule:evenodd;" \
                "stroke:#445500;" \
                "stroke-width:38.13341522;" \
                "stroke-miterlimit:4;" \
                "stroke-dasharray:none;" \
                "stroke-dashoffset:0;" \
                "stroke-opacity:1;" \
                "image-rendering:optimizeQuality;" \
                "shape-rendering:geometricPrecision;" \
                "text-rendering:geometricPrecision"

    circleElem.set("style", style)
    circleElem.set("cx", '{:.6f}'.format(poi.x))
    circleElem.set("cy", '{:.6f}'.format(poi.y))
    circleElem.set("r", str(r))
    circleElem.set("id", poi.id)
    circleElem.set(NS_ink + "label", poi.name)


def addNewLayer(lyrid, lyrName, root):
    lyr = ET.SubElement(root, NS0 + 'g')
    lyr.attrib.update({NS_ink + 'groupmode': 'layer'})
    lyr.attrib.update({NS_ink + 'label': lyrName})
    lyr.attrib.update({'id': lyrid})
    return lyr


def setLayerShow(lyrName, root, isShow=True):
    pass


def addPolylineElement(lyrElem, pid, pnlist):
    poly = ET.SubElement(lyrElem, NS0 + 'polyline')
    poly.set('id', pid)
    drawPolyline(poly, pnlist)
    return poly


def addPOIElementsFromList(lyrElem, poilist, r=50):
    for poi in poilist:
        circleElem = ET.SubElement(lyrElem, NS0 + 'circle')
        drawPOI(circleElem, poi, r)


def loadPOI2LayerFromCSV(root, lyrTag, lyrName, csvfile):
    lyr = addNewLayer(lyrTag, lyrName, root)

    # For HDBC3: kx=29600, ky=21400, dx=0, dy=0, theta=0.2, r=60
    poilist = CSV2POIlist(csvfile, MTKX, MTKY, MTDX, MTDY, MTTHETA)
    addPOIElementsFromList(lyr, poilist, r=100)
    return poilist


def addAllPOIsInLayerToGraph(root, lyrName, graph):
    poilist = LoadElementInLayer(lyrName, root)
    poidict = {}
    for p in poilist:
        if 'circle' in p.tag:
            # TODO: need create a POI but not a Pathnode
            # poi = circle2Node(p)
            poi = circle2POINode(p)
            if graph.checkNodeInbyID(poi):
                poi.id += '_a'
            graph.addPOI(poi)
    return graph


def addAllPathsInLayerToGraph(root, lyrName, graph):
    pathlist = LoadElementInLayer(lyrName, root)
    pnL_dict = {}
    for p in pathlist:
        if 'path' in p.tag:
            ptL = Path2PointList(p)
            pnL = []
            if 'id' in p.attrib:
                pnL = ptList2nodeList(ptL, p.attrib['id'])
            else:
                pnL = ptList2nodeList(ptL, 'path')

            for pn in pnL:
                if graph.checkNodeInbyID(pn):
                    pn.id += '_b'
            graph.appendFromPolyline(pnL)
    return graph


def turnAllPathToPolyline(root, lyrName):
    pathlist = LoadElementInLayer(lyrName, root)
    for p in pathlist:
        if 'path' in p.tag:
            Path2Polyline(p)


def circle2Node(circleElem):
    cx = float(circleElem.attrib['cx'])
    cy = float(circleElem.attrib['cy'])
    # r = float(circleElem.attrib['r'])
    cid = circleElem.attrib['id']
    return PathNode(Point(cx, cy), cid)


def circle2POINode(circleElem):
    cx = float(circleElem.attrib['cx'])
    cy = float(circleElem.attrib['cy'])
    cid = circleElem.attrib['id']
    if NS_ink + 'label' in circleElem.attrib:
        poiname = circleElem.attrib[NS_ink + 'label']
    else:
        poiname = 'unknown'
    poi = POI(Point(cx, cy), cid)
    poi.setName(poiname)
    return poi


def CheckNodeIDUniqueness(nodelist):
    repeatnodelist = {}
    for n in nodelist:
        if n.id in repeatnodelist:
            repeatnodelist[n.id] += 1
        else:
            repeatnodelist.update({n.id: 1})
    return {k: v for k, v in repeatnodelist.items() if v > 1}


def Path2PointList(pathelem):
    d = pathelem.attrib['d']
    path = parse_path(d)
    ptList = []
    for node in path:
        if 'Move' in str(node) or 'Line' in str(node):
            ptList.append(Point(str(node.end.real), str(node.end.imag)))
        else:
            print('ERROR! cannot support: ' + d)
            return []
    return ptList


def ptList2nodeList(ptList, pathid):
    return [PathNode(pt, pathid + '_' + str(i)) for i, pt in enumerate(ptList)]


def DumpPOI(root, filename):
    poiL = LoadElementInLayer('POI', root)
    data = []
    POIList = []
    for poi in poiL:
        p = {'id': poi.attrib['id'],
             'x': poi.attrib['cx'],
             'y': poi.attrib['cy'],
             'name': poi.attrib[NS_ink + 'label']}

        poi_obj = POI(Point(float(poi.attrib['x']), float(poi.attrib['y'])),
                      poi.attrib['id'])
        poi_obj.setName(poi.attrib[NS_ink + 'label'])

        data.append(p)
        POIList.append(poi_obj)

    with open(filename, 'w') as fp:
        fp.write(json.dumps(data))
    return POIList


def DumpPathNode(root, filename):
    pathL = LoadElementInLayer('Path', root)
    data = []
    PathNodeList = []

    for path in pathL:
        if path.tag == NS0 + 'path':
            PtList = Path2PointList(path)
            print('{} pts - ID:{}, d:{}'.format(len(PtList), path.attrib['id'], path.attrib['d']))
            print([str(pt) for pt in PtList])
    pass


def dumpAllPOIRoutes(graph, filepath):
    routes = {}
    poilist = graph.getPOIs()
    for i, poi in enumerate(poilist):
        data = graph.dumpGraph()
        print('({}/{}) Exporting routes for POI {}'.format(i, len(poilist), poi))
        distance, path, loops = dijkstra(data, poi.id)
        print('{} loops cost'.format(loops))
        routes.update(path)

    with open(filepath, 'w') as fp:
        fp.write(json.dumps(routes))
    pass


def QueryRouteFromFile(src, filepath):
    data = {}
    with open(filepath, 'r') as fp:
        data = json.load(fp)
    return data[src]


def QueryNodesFromFile(filepath):
    nodes = {}
    with open(filepath, 'r') as fp:
        nodes = json.load(fp)
    return nodes


def PathnodeID2PNList(pnidlist, nodes):
    pnlist = []
    for pnid in pnidlist:
        pn = PathNode(Point(nodes[pnid][0], nodes[pnid][1]), pnid)
        pnlist.append(pn)
    return pnlist


def isNodeIn(node, nodeList):
    for n in nodeList:
        if node.isSame(n):
            return True
    return False


# === All tests ===

def testNode(eps=EPS):
    pt0 = Point(0, 0)
    pn0 = PathNode(pt0, 'PN0')
    pt1 = Point(0, 1)
    pn1 = PathNode(pt1, 'PN1')
    # print(pn1.isSame(pt0, eps))
    # print(pn1.isSame(pt1, eps))

    # ------------------------------------
    ptlist = []
    pnlist = []
    ptset = set(ptlist)
    pnset = set(pnlist)
    for i in range(10):
        pt = Point(i, i + 1)
        pn = PathNode(pt, 'pn#' + str(i))
        ptlist.append(pt)
        ptset.add(pt)
        pnlist.append(pn)
        pnset.add(pn)
        ptset.add(pt)
    print(pnlist[0])
    print('{} in pnset: {}'.format(str(pn0), isNodeIn(pn0, pnset)))
    print('{} in pnset: {}'.format(str(pn1), isNodeIn(pn1, pnset)))
    pass


def testGraph(pnlist, src='', dest=''):
    g = Graph(EPS)
    g.appendFromPolyline(pnlist)
    print('g.nodes:{}, {}'.format(len(g.nodes), g.nodes.keys()))
    print('g.edges:{}'.format(len(g.edges)))
    for i in g.edges:
        print(g.edges[i])

    g.updateAllNodeLinks()
    data = g.dumpGraph()
    with open("graph.json", 'w') as fp:
        fp.write(json.dumps(data))

    if not src:
        src = pnlist[0].id
    distance, path, t = dijkstra(data, src)
    print("\nSource= " + src + " in " + str(t) + " times loop.")
    print(distance)
    print(path)
    print(t)

    if src and dest:
        return [src] + path[src][dest]
    else:
        return []
    pass


def testSVG():
    filename = 'TestLine.svg'
    tree = LoadSVG(filename)
    root = tree.getroot()

    # for g in root.iter(NS0 + 'g'):
    #     if NS_ink + 'groupmode' in g.attrib:
    #         print(g.attrib)
    # pass

    # for lyr in root.findall(".//*[@{http://www.inkscape.org/namespaces/inkscape}label='POI']"):
    #     for e in lyr:
    #         print (e.tag, e.attrib)

    # for e in LoadElementInLayer('POI',root):
    #     print (e.tag, e.attrib)

    # Ellipse2circle(root,2)
    # UnifyCircle(root,3.2)

    pathlist = LoadElementInLayer('Path', root)
    all_ptL = []
    all_pnL = []
    all_pnL_dict = {}
    for p in pathlist:
        if 'path' in p.tag:
            ptL = Path2PointList(p)
            if 'id' in p.attrib:
                pnL = ptList2nodeList(ptL, p.attrib['id'])
            else:
                pnL = ptList2nodeList(ptL, 'path')
            all_ptL += ptL
            all_pnL += pnL
            all_pnL_dict.update({pn.id: pn for pn in pnL})

            for pn in pnL:
                print(pn)
            # Path2Polyline(p)
    print('{} nodes are found.\n'.format(len(all_pnL)))

    planids = testGraph(all_pnL, 'path6494_0', 'path6492_1')
    plan_pnL = [all_pnL_dict[pid] for pid in planids]

    lyr = addNewLayer('layer_new', 'plan', root)
    poly = addPolylineElement(lyr, 'Polyline01', plan_pnL)
    tree.write("Result.svg")


def loadGraphfromSVG(svgname, isDumpData=True):
    # Init graph
    g = Graph(EPSDict[svgname])
    # Read SVG file
    tree = LoadSVG(svgname)
    root = tree.getroot()

    # Unify POIs into same shape in SVG
    Ellipse2circle(root)
    UnifyCircle(root)

    # Add all POIs to graph
    g = addAllPOIsInLayerToGraph(root, 'room', g)
    g = addAllPOIsInLayerToGraph(root, 'poi', g)

    # Add all POILink to graph
    g = addAllPathsInLayerToGraph(root, 'poi-link', g)
    turnAllPathToPolyline(root, 'poi-link')

    # Add all Path to graph
    g = addAllPathsInLayerToGraph(root, 'path', g)
    turnAllPathToPolyline(root, 'path')

    # Update PathNode links
    g.updateAllNodeLinks()

    # TODO: Do some check for graph
    # Calculate linked-node-set number. 1 set means all nodes are linked
    union = CheckUnion(g)
    union.unionAllEdges()
    print('Check nodes <=> edge_ends: {}'.format(union.checkNodesEdges()))
    print('{} node sets in the graph'.format(union.getUninN()))

    # print('--------------- all edge ---------------')
    # for edge in g.dumpEdges():
    #     print(edge)
    # print('--------------- all nodes ---------------')
    # for node in g.dumpNodes():
    #     print(node)
    # print('--------------- all pois ---------------')
    # for poi in g.dumpPOIs():
    #     print(poi)

    if isDumpData:
        # Save all POIs in the Graph to a json file
        poidict = g.dumpPOIID_NameDict()
        jsonName = svgname.split('.')[0] + '_poi.json'
        with open(jsonName, 'w') as fp:
            fp.write(json.dumps(poidict))

        # Save all nodes in a graph to a json file
        jsonName = svgname.split('.')[0] + '_nodes.json'
        nodesdict = g.dumpNodesDict()
        with open(jsonName, 'w') as fp:
            fp.write(json.dumps(nodesdict))

        # Save all routes in a graph to a json file
        jsonName = svgname.split('.')[0] + '_routes.json'
        dumpAllPOIRoutes(g, jsonName)

    return g, tree


def testPlan(svgname, src, dest1, dest2):
    # Load info from svg file
    g, tree = loadGraphfromSVG(svgname)
    root = tree.getroot()

    # src = 'poi001'
    # dest1 = 'poi006'
    # dest2 = 'poi005'

    # Route Planning
    data = g.dumpGraph()
    distance, path, loops = dijkstra(data, src)
    # print (path)
    # print (distance)

    route1 = [src] + path[src][dest1]
    print('route1: ', route1)
    route2 = [src] + path[src][dest2]
    print('route2: ', route2)

    route_pnL1 = g.queryNodes(route1)
    route_pnL2 = g.queryNodes(route2)

    # Add layer and polyline to result svg
    lyr = addNewLayer('layer_new1', 'plan1', root)
    poly1 = addPolylineElement(lyr, 'Polyline01', route_pnL1)
    lyr = addNewLayer('layer_new2', 'plan2', root)
    poly2 = addPolylineElement(lyr, 'Polyline02', route_pnL2)

    print('{} nodes in route1. From {} to {}'
          .format(len(route_pnL1),
                  route_pnL1[0].id,
                  route_pnL1[-1].id))

    tree.write("Result.svg")


def testQueryPath(src, dest, pathjson, nodesjson, svgname):
    path = QueryRouteFromFile(src, pathjson)
    print('From {} to {}: {}'.format(src, dest, path[dest]))
    print('All routes from {}: {}'.format(src, path))
    print('Route: {}'.format([src] + path[dest]))

    nodes = QueryNodesFromFile(nodesjson)
    rouetePnList = PathnodeID2PNList([src] + path[dest], nodes)

    tree = LoadSVG(svgname)
    root = tree.getroot()
    # Add layer and polyline to result svg
    lyr = addNewLayer('layer_new1', 'plan1', root)
    poly1 = addPolylineElement(lyr, src + '_' + dest, rouetePnList)
    tree.write("QueryResult.svg")


def testLoadPOI(csvfile, svgname):
    tree = LoadSVG(svgname)
    root = tree.getroot()
    poilist = loadPOI2LayerFromCSV(root, 'csvpoi', 'room', csvfile)
    tree.write("LoadResult.svg")
    print('{} poi are loaded into {}.'.format(len(poilist), svgname))


def main():
    # filename = 'TestLine.svg'
    # tree = LoadSVG(filename)
    # root = tree.getroot()

    # testSVG()
    # testNode()
    # testGraph()
    # testPlan('TestLine.svg', 'poi001', 'poi003', 'poi002'a)
    # testPlan('HDBC3.svg', 'poi002', 'poi004', 'poi005')
    testPlan('HDBC3.svg', 'room-hd-3c-i-suzhouting',
             'room-hd-3b-q-foshanting', 'room-hd-3b-j-zhangzhouting')
    # testPlan('WJE2.svg', 'room-bjyfyE-2f-exit',
    #          'room-bjyfyE-2f-f-fuzhouting', 'mis_wangminghui08')
    # testQueryPath('poi002', 'poi004', 'HDBC3_routes.json', 'HDBC3_nodes.json', 'HDBC3_basemap.svg')
    # testLoadPOI('HDBC3_info.csv', 'HDBC3a.svg')
    # testLoadPOI('WJE2.csv', 'WJE2.svg')
    # data002 = QueryRouteFromFile('poi002', 'HDBC3_routes.json')
    # print(data002)

    # DumpPOI(root, 'poi.json')
    # DumpPathNode(root,'PathNode.json')
    pass


if __name__ == '__main__':
    main()
    pass
