class Point(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __str__(self):
        return "P({:.2f},{:.2f})".format(self.x, self.y)

    def dist(self, point):
        return ((self.x - point.x) ** 2 + (self.y - point.y) ** 2) ** 0.5

    def distFast(self, point):
        return abs((self.x - point.x)) + abs(self.y - point.y)


class PathNode(Point):
    def __init__(self, point, nid):
        self.x = point.x
        self.y = point.y
        self.id = str(nid)
        self.linkedpoi = "none"
        self.linkNodes = set([])

    def __str__(self):
        return "{}:P({:.2f},{:.2f})".format(str(self.id), self.x, self.y)

    def setLinkedPoi(self, nid):
        self.linkedpoi = nid

    def getLinkedPoi(self):
        return self.linkedpoi

    def addLink(self, node):
        self.linkNodes.add(node)

    def removeLink(self, node):
        self.linkNodes.discard(node)

    def isSame(self, point, eps):
        return self.dist(point) < eps

    def isSameFast(self, point, eps):
        return self.distFast(point) < eps


class POI(PathNode):
    def __init__(self, point, nid):
        PathNode.__init__(self, point, nid)
        self.linkedpoi = "self"
        self.poiid = nid
        self.name = ''

    def setName(self, name):
        self.name = name

    def setPOIID(self, poiid):
        self.poiid = poiid


class Edge(object):
    def __init__(self, node0, node1):
        self.start = node0
        self.end = node1
        self.length = node0.dist(node1)
        self.id = ''

    def __str__(self):
        return '#{}<{}->{}>' \
            .format(str(self.id), str(self.start), str(self.end))

    def setid(self, eid):
        self.id = str(eid)

    def setlength(self, length):
        self.length = length

    def getNodeIds(self):
        return [str(self.start.id), str(self.end.id)]

    def isSame(self, edge):
        return edge.start.isSame(edge.end)


class Graph(object):
    def __init__(self, eps):
        self.NidCnt = 0
        self.EidCnt = 0
        self.nodes = {}
        self.edges = {}
        self.pois = {}
        self.eps = eps

    def checkNodeIn(self, node):
        for id in self.nodes.keys():
            if node.isSame(self.nodes[id], self.eps):
                return id
        return 'NotHere'

    def checkNodeInbyID(self, node):
        return node.id in self.nodes.keys()

    def checkEdgeIn(self, edge):
        pass

    def checkBadEdge(self):
        for e in self.edges:
            if self.edges[e].start == self.edges[e].end:
                print('Warning: A bad edge: {}'.format(str(self.edges[e])))

    def findEdgeByNodeID(self, nid0, nid1):
        for eid in self.edges:
            if set(self.edges[eid].getNodeIds()) == set([nid0, nid1]):
                return self.edges[eid]
        return None

    def setEdgeLength(self, nid0, nid1, length):
        edge = self.findEdgeByNodeID(nid0, nid1)
        if edge:
            edge.setLength(length)
            return True
        else:
            return False

    # ID from 1
    def genNodeID(self):
        self.NidCnt += 1
        return self.NidCnt

    def genEdgeID(self):
        self.EidCnt += 1
        return self.EidCnt

    def appendFromPolyline(self, nodelist):
        if not nodelist:
            print("ERROR: Node list is Empty")
            return False

        last_node = None
        for n in nodelist:
            nid = self.checkNodeIn(n)
            if nid == 'NotHere':
                self.nodes.update({n.id: n})
            elif nid in self.pois.keys():
                n = self.pois[nid]
                if last_node and last_node.getLinkedPoi() != 'self':
                    last_node.setLinkedPoi(n.poiid)
            else:
                self.syncDataWithNewNode(n, nid)
                n = self.nodes[nid]

            if last_node and (n != last_node):
                if last_node.getLinkedPoi() == 'self' and n.getLinkedPoi() != 'self':
                    n.setLinkedPoi(last_node.poiid)
                e = Edge(n, last_node)
                e.setid(self.genEdgeID())
                self.edges.update({e.id: e})
            pass
            last_node = n
        pass

    def syncDataWithNewNode(self, newNode, oldNodeId):
        if self.nodes[oldNodeId].getLinkedPoi() == 'none':
            self.nodes[oldNodeId].setLinkedPoi(newNode.getLinkedPoi())

    # TODO: need type check? poi vs pathnode
    def addPOI(self, poinode):
        self.pois.update({poinode.id: poinode})
        self.nodes.update({poinode.id: poinode})
        pass

    def updateAllNodeLinks(self):
        for e in self.edges.values():
            e.start.addLink(e.end)
            e.end.addLink(e.start)
        pass

    def getEdges(self):
        return self.edges.values()

    def getNodes(self):
        return self.nodes.values()

    def dumpNodesDict(self):
        return {n.id: [n.x, n.y, n.getLinkedPoi()] for n in self.nodes.values()}

    def dumpPOIID_NameDict(self):
        return {p.id: p.name for p in self.pois.values()}

    def getPOIs(self):
        return self.pois.values()

    def queryNodes(self, nodeidlist):
        return [self.nodes[nid] for nid in nodeidlist]

    def dumpGraph(self):
        GraphDict = {}
        for n in self.nodes.values():
            links = {n.id: 0}
            # Update linked nodes
            for nei in n.linkNodes:
                links.update({nei.id: nei.dist(n)})
            # update no-linked nodes
            for nid in self.nodes.keys():
                if nid not in links:
                    links.update({nid: 'inf'})
            GraphDict.update({n.id: links})
        return GraphDict
