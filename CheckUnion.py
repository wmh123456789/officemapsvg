class CheckUnion(object):
    def __init__(self, graph):
        self.nodes = graph.nodes
        self.edges = graph.edges
        self.parent = {self.nodes[n].id: 'unknown' for n in self.nodes}
        pass

    def checkNodesEdges(self):
        endSet = set([])
        for e in self.edges:
            endSet.add(self.edges[e].start.id)
            endSet.add(self.edges[e].end.id)
        return set(self.nodes.keys()) == endSet

    def find(self, nodeid):
        if self.parent[nodeid] == 'unknown':
            return nodeid
        return self.find(self.parent[nodeid])

    def union(self, nodex, nodey):
        xset = self.find(nodex)
        yset = self.find(nodey)
        if xset > yset:
            self.parent[xset] = yset
        elif xset < yset:
            self.parent[yset] = xset

    def unionAllEdges(self):
        for e in self.edges:
            if self.edges[e].start == self.edges[e].end:
                print('Find a bad edge: {}'.format(str(self.edges[e])))
            else:
                self.union(self.edges[e].start.id, self.edges[e].end.id)
        pass

    def getUionTree(self):
        return self.parent

    def getUninN(self):
        return len([u for u in self.parent.values() if u == 'unknown'])


if __name__ == '__main__':
    pass
