def dijkstra(graph, src):
    length = len(graph)
    type_ = type(graph)
    if type_ == list:
        nodes = [i for i in range(length)]
    elif type_ == dict:
        nodes = list(graph.keys())

    visited = [src]
    path = {src: {src: []}}
    nodes.remove(src)
    distance_graph = {src: 0}
    pre = next = src

    calcTime = 0
    while nodes:
        distance = float('inf')
        for v in visited:
            for d in nodes:
                calcTime += 1
                new_dist = float(graph[src][v]) + float(graph[v][d])
                if new_dist <= distance:
                    distance = new_dist
                    next = d
                    pre = v
                    graph[src][d] = new_dist

        if graph[src][next] == float('inf'):
            path[src][next] = []
        else:
            path[src][next] = [i for i in path[src][pre]]
            path[src][next].append(next)

        distance_graph[next] = distance

        visited.append(next)
        nodes.remove(next)

    return distance_graph, path, calcTime


def main():
    graph_list = [[0, 2, 1, 4, 5, 1],
                  [1, 0, 4, 2, 3, 4],
                  [2, 1, 0, 1, 2, 4],
                  [3, 5, 2, 0, 3, 3],
                  [2, 4, 3, 4, 0, 1],
                  [3, 4, 7, 3, 1, 0]]
    #
    # graph_dict = {"s1": {"s1": 0, "s2": 2, "s10": 1, "s12": 4, "s5": 3},
    #               "s2": {"s1": 1, "s2": 0, "s10": 4, "s12": 2, "s5": 2},
    #               "s10": {"s1": 2, "s2": 1, "s10": 0, "s12": 1, "s5": 4},
    #               "s12": {"s1": 3, "s2": 5, "s10": 2, "s12": 0, "s5": 1},
    #               "s5": {"s1": 3, "s2": 5, "s10": 2, "s12": 4, "s5": 0},
    #               }

    MAX = float('inf')
    graph_dict = {
        'a': {'a': 0, 'b': 1, 'c': 'inf', 'd': 'inf', 'e': 'inf', 'f': 'inf'},
        'b': {'a': 1, 'b': 0, 'c': 1, 'd': 'inf', 'e': 22, 'f': 'inf'},
        'c': {'a': 'inf', 'b': 1, 'c': 0, 'd': 'inf', 'e': 'inf', 'f': 1},
        'd': {'a': 'inf', 'b': 'inf', 'c': 'inf', 'd': 0, 'e': 1, 'f': 'inf'},
        'e': {'a': 'inf', 'b': 22, 'c': 'inf', 'd': 1, 'e': 0, 'f': 1},
        'f': {'a': 'inf', 'b': 'inf', 'c': 1, 'd': 'inf', 'e': 1, 'f': 0}
    }

    g_dict = {
        'a': {'a': 0, 'b': 1, 'c': 'inf', 'd': 'inf', 'e': 'inf'},
        'b': {'a': 1, 'b': 0, 'c': 1, 'd': 'inf', 'e': 'inf'},
        'c': {'a': 'inf', 'b': 1, 'c': 0, 'd': 'inf', 'e': 'inf'},
        'd': {'a': 'inf', 'b': 'inf', 'c': 'inf', 'd': 0, 'e': 1},
        'e': {'a': 'inf', 'b': 'inf', 'c': 'inf', 'd': 1, 'e': 0}
    }

    graph_dict2 = {
        "pn#0": {"pn#0": 0, "pn#2": 2.8284271247461903, "pn#1": 1.4142135623730951, "pn#3": "inf", "pn#4": "inf",
                 "pn#5": "inf", "pn#6": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#1": {"pn#1": 0, "pn#2": 1.4142135623730951, "pn#0": 1.4142135623730951, "pn#3": "inf", "pn#4": "inf",
                 "pn#5": "inf", "pn#6": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#2": {"pn#2": 0, "pn#0": 2.8284271247461903, "pn#4": 2.8284271247461903, "pn#1": 1.4142135623730951,
                 "pn#3": 1.4142135623730951, "pn#5": "inf", "pn#6": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#3": {"pn#3": 0, "pn#2": 1.4142135623730951, "pn#4": 1.4142135623730951, "pn#0": "inf", "pn#1": "inf",
                 "pn#5": "inf", "pn#6": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#4": {"pn#4": 0, "pn#2": 2.8284271247461903, "pn#5": 1.4142135623730951, "pn#6": 2.8284271247461903,
                 "pn#3": 1.4142135623730951, "pn#0": "inf", "pn#1": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#5": {"pn#5": 0, "pn#6": 1.4142135623730951, "pn#4": 1.4142135623730951, "pn#0": "inf", "pn#1": "inf",
                 "pn#2": "inf", "pn#3": "inf", "pn#7": "inf", "pn#8": "inf", "pn#9": "inf"},
        "pn#6": {"pn#6": 0, "pn#7": 1.4142135623730951, "pn#5": 1.4142135623730951, "pn#8": 2.8284271247461903,
                 "pn#4": 2.8284271247461903, "pn#0": "inf", "pn#1": "inf", "pn#2": "inf", "pn#3": "inf", "pn#9": "inf"},
        "pn#7": {"pn#7": 0, "pn#6": 1.4142135623730951, "pn#8": 1.4142135623730951, "pn#0": "inf", "pn#1": "inf",
                 "pn#2": "inf", "pn#3": "inf", "pn#4": "inf", "pn#5": "inf", "pn#9": "inf"},
        "pn#8": {"pn#8": 0, "pn#6": 2.8284271247461903, "pn#7": 1.4142135623730951, "pn#9": 1.4142135623730951,
                 "pn#0": "inf", "pn#1": "inf", "pn#2": "inf", "pn#3": "inf", "pn#4": "inf", "pn#5": "inf"},
        "pn#9": {"pn#9": 0, "pn#8": 1.4142135623730951, "pn#0": "inf", "pn#1": "inf", "pn#2": "inf", "pn#3": "inf",
                 "pn#4": "inf", "pn#5": "inf", "pn#6": "inf", "pn#7": "inf"}}

    # distance, path = dijkstra(graph_list, 2)
    # print distance, '\n', path
    # src = 'a'
    src = 'a'
    distance, path, t = dijkstra(g_dict, src)
    print("Source= " + src + " in " + str(t) + " times loop.")
    print(distance)
    print(path)


if __name__ == '__main__':
    # a = ['1','2','3']
    # a.remove('2')
    # print(a)
    main()
    # a = [1,1]
    # b = [0,0]
    # dist = (float(a[0]-b[0])**2 + float(a[1]-b[1])**2)**0.5
    # print('Distance = {}'.format(dist))

    pass
