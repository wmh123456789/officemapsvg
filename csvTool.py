# coding: utf-8
import csv
from math import cos, sin

from Graph import *


def loadCSV(filename):
    roomDict = {}
    with open(filename, newline='', encoding='utf-8') as fp:
        f_csv = csv.DictReader(fp)
        for row in f_csv:
            name = row['room_name'].split('F')[-1]
            if name in roomDict:
                print('Find a duplicate room name:' + name)
                name += '_a'
            flname = row['floor_name']
            email = row['email'].split('@')[0]
            x = row['point_x']
            y = row['point_y']
            roomDict.update({email: {'flname': flname,
                                     'name': name,
                                     'x': float(x),
                                     'y': float(y)}})
            # print(row)
    return roomDict


def roomname2id(name):
    rid = ''
    return rid


def transform2d(srcPt, x_ratio, y_ratio, dx, dy, theta):
    PI = 3.14159
    theta = theta*PI/180.0
    xx = srcPt.x * x_ratio
    yy = srcPt.y * y_ratio
    x =  xx*cos(theta)+yy*sin(theta)
    y = -xx*sin(theta)+yy*cos(theta)
    return Point(x+dx, y+dy)


# Pnew = zoomfactor*P + (dx,dy)
def CSV2POIlist(csvfile, x_ratio=1.0, y_ratio=1.0, dx=0.0, dy=0.0, theta=0.0):
    roomdict = loadCSV(csvfile)
    poilist = []
    for roomid, info in roomdict.items():
        pt = transform2d(Point(info['x'],info['y']), x_ratio, y_ratio, dx, dy, theta)
        poi = POI(pt, roomid)
        poi.setName(info['name'])
        poilist.append(poi)
    return poilist


def main():
    roomdict = loadCSV('HDBC3_info.csv')
    for roomid, info in roomdict.items():
        print('{}:{} at [{},{}] on {}'.format(roomid,
                                              info['name'],
                                              info['x'], info['y'],
                                              info['flname']))
    pass


if __name__ == '__main__':
    main()
